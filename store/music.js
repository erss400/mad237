import axios from 'axios'
import Cookies from 'js-cookie'

// state
export const state = () => ({
  albums: [],
  songs: [],
  songMeta: {},
  albumMeta: {}
})

// getters
export const getters = {
  albums: state => state.albums,

  songs: (state, getters) => {
    return state.songs.map(songItem => {
        return {
          id: songItem.id,
          title: songItem.title,
          author: songItem.profile.data.stageName,
          artistSlug: songItem.profile.data.slug,
          src: songItem.src,
          pic: songItem.pic,
          albumTitle: songItem.album.data.title,
          albumSlug: songItem.album.data.slug,
          lyric: songItem.lyric,
          format: 'mp3',
        }
    })
  },

  album: (state) => (slug) => {
    if (typeof state.albums !== 'undefined' && state.albums.length > 0) {
      // the array is defined and has at least one element
       return state.albums.find((album) => {
        return album.slug === slug
      })
    }
    else {
      return null
    }
  },

  songMeta: state => state.songMeta
}

// mutations
export const mutations = {

  FETCH_ALBUMS_SUCCESS (state, albums) {
    state.albums = albums
  },

  FETCH_ALBUMS_FAILURE (state) {
    state.albums = []
  },

  FETCH_SONGS_SUCCESS (state,  songs) {
    state.songs = songs
  },

  FETCH_SONGS_FAILURE (state) {
    state.songs = []
  },

  SET_SONGS_META (state, pagination) {
    state.songMeta = pagination
  },

  SET_ALBUMS_META (state, pagination) {
    state.albumMeta = pagination
  }
}

// actions
export const actions = {
  async fetchAlbums ({ commit }) {
    try {
      const { data } = await axios.get(process.env.musicApi + '/get/ablums')

      commit('FETCH_ALBUMS_SUCCESS', data.data)
      commit('SET_ALBUMS_META', data.meta.pagination)
    } catch (e) {

      commit('FETCH_ALBUMS_FAILURE')
    }
  },
  async fetchSongs ({ commit }) {
    try {
      const { data } = await axios.get(process.env.musicApi + '/get/songs')

      commit('FETCH_SONGS_SUCCESS', data.data)
      commit('SET_SONGS_META', data.meta.pagination)
    } catch (e) {

      commit('FETCH_SONGS_FAILURE')
    }
  },

  async fetchMoreSongs ({commit, dispatch, state}, {url}) {
    try {
     let response =  await axios.get(url)
     dispatch('mergeSongs', {data: response.data.data})
     commit('SET_SONGS_META', response.data.meta.pagination)
    } catch (e) { 
      console.log(e)
      commit('FETCH_SONGS_FAILURE')
    }
  },

  mergeSongs ({commit, dispatch, state}, {data}) {
    try {
      const result = state.songs.concat(data)
      commit('FETCH_SONGS_SUCCESS', result)
    } catch(e) {
      // statements
      console.log(e)
      commit('FETCH_SONGS_FAILURE')
    }
  },

  async fetchMoreAlbums ({commit, dispatch, state}, {url}) {
    try {
     let response =  await axios.get(url)
     dispatch('mergeAlbums', {data: response.data.data})
     commit('SET_ALBUMS_META', response.data.meta.pagination)
    } catch (e) { 
      console.log(e)
      commit('FETCH_SONGS_FAILURE')
    }
  },

  mergeAlbums ({commit, dispatch, state}, {data}) {
    try {
      const result = state.albums.concat(data)
      commit('FETCH_ALBUMS_SUCCESS', result)
    } catch(e) {
      // statements
      console.log(e)
      commit('FETCH_ALBUMS_FAILURE')
    }
  }
}
