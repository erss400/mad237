import axios from 'axios'
import Cookies from 'js-cookie'

// state
export const state = () => ({
  createShow: false,
  editShow: false,
  playlists: [],
})

// getters
export const getters = {
  createShow: state => state.createShow,
  editShow: state => state.editShow,
  playlists: state => state.playlists,
}

// mutations
export const mutations = {

  SET_CREATE_SHOW (state) {
    state.createShow = !state.createShow
  },

  SET_EDIT_SHOW (state) {
    state.editShow = !state.editShow
  },

  SET_PLAYLISTS (state, playlists) {
    state.playlists = playlists
  },

  SET_CREATE_PLAYLIST (state, playlist) {
    state.playlists.push(playlist)
  }
}

// actions
export const actions = {
  setCreateShow ({ commit, dispatch }) {
    commit('SET_CREATE_SHOW')
  },

  setEditShow ({commit, dispatch}) {
    commit('SET_EDIT_SHOW')
  },

  setPlaylist ({commit, dispatch}, playlists) {
    commit('SET_PLAYLISTS', playlists)
  },

  createPlaylist ({commit, dispatch}, playlist) {
    commit('SET_CREATE_PLAYLIST', playlist)
  }
}
